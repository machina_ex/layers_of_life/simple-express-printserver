const fs = require('fs'),
    exec = require('child_process').exec;

const express = require('express')
const app = express()
const port = 8081

// TEST Print
app.get('/', (req, res) => {
    exec("lp ./assets/test.txt",(res)=>{
        console.log(res);
    });
})

// PRINT FILE BY FILENAME
app.get('/print/:filename', (req, res) => {
    console.log("printing ",req.params.filename);
    exec("lp ./assets/"+req.params.filename,(res)=>{
        console.log(res)
    });
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})

