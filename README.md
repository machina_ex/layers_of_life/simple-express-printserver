## Usage

- install with `npm i`
- change port if necessary
- `node server.js`
- SEND HTTP GET request at `/print/:filename` endpoint where :filename is the name of the file to be printed. 
- It will standard printer if one is defined, to print the specified file from `/assets/`